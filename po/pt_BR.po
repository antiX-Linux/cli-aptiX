# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR antiX Linux
# This file is distributed under the same license as the antiX Development package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
# Translators:
# James Bowlin <BitJam@gmail.com>, 2017
# cmenemrs <cmenemrs@gmail.com>, 2018
# marcelo cripe <marcelocripe@gmail.com>, 2024
#
msgid ""
msgstr ""
"Project-Id-Version: antiX Development\n"
"Report-Msgid-Bugs-To: translation@antixlinux.org\n"
"POT-Creation-Date: 2019-06-10 12:10-0600\n"
"PO-Revision-Date: 2024-03-13 19:18+0200\n"
"Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2024\n"
"Language-Team: Portuguese (Brazil) (https://app.transifex.com/"
"anticapitalista/teams/10162/pt_BR/)\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % "
"1000000 == 0 ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. "aptiX" is the short name of this program
msgid "Press 'q' to return to aptiX"
msgstr "Pressione a tecla ‘q’ para voltar ao gerenciador de pacotes aptiX"

#. The category of command line programs
msgid "Command Line"
msgstr "Linha de Comando"

#. The catatory of GUI (graphics user interface) programs
msgid "GUI"
msgstr "Interface Gráfica de Usuário"

#. Keyboard keys: up-arrow, down-arrow, page-up, page-down
msgid "up-arrow"
msgstr "seta para cima"

msgid "down-arrow"
msgstr "seta para baixo"

msgid "page-up"
msgstr "página para cima"

msgid "page-down"
msgstr "página para baixo"

#. First letter is lowercase
msgid "go back to the main menu"
msgstr "Voltar ao menu principal"

msgid "antiX Command Line Package Installer"
msgstr "Gerenciador de Programas por Interface de Texto do antiX"

msgid "Main Menu"
msgstr "Menu Principal"

msgid "last update"
msgstr "última atualização"

msgid "Search for packages to mark or install"
msgstr "Procurar programas ou pacotes para marcar ou instalar"

msgid "Search for antiX kernels"
msgstr "Procurar por núcleos (kernels) do antiX"

msgid "Search for all kernels"
msgstr "Procurar todos os núcleos (kernels)"

#. menu_printf kernels   $"Search for kernels or antiX kernels"
msgid "Update package index"
msgstr "Atualizar a lista de pacotes"

msgid "Edit the repo source files"
msgstr "Editar os arquivos de origem dos repositórios"

msgid "Quit"
msgstr "Sair"

#. all <25> suggested <GUI> packages have been installed
msgid "All %s suggested %s packages have been installed"
msgstr "Todos os %s pacotes %s sugeridos foram instalados"

msgid "no pending upgrades"
msgstr "Não há atualizações pendentes"

#. View a list of <32> suggested <Command Line> packages
msgid "View a list of %s suggested %s packages"
msgstr "Exibir uma lista de %s pacotes %s sugeridos"

msgid "View or perform %s upgrade now"
msgstr "Exibir ou executar agora %s atualização"

msgid "View or perform %s upgrades now"
msgstr "Exibir ou executar agora %s atualizações"

msgid "Upgrade %s package now"
msgstr "Atualizar agora %s pacote"

msgid "Upgrade %s packages now"
msgstr "Atualizar agora %s pacotes"

msgid "View a list of packages to be upgraded"
msgstr "Exibir uma lista de pacotes a serem atualizados"

msgid "Ignore these upgrades for now"
msgstr "Ignorar estas atualizações por enquanto"

msgid "View or install %s marked package"
msgstr "Exibir ou instalar o pacote %s marcado"

msgid "View or install %s marked packages"
msgstr "Exibir ou instalar os pacotes %s marcados"

msgid "Edit Source Menu"
msgstr "Editar o Menu de Origens"

msgid "Please select a source file to edit"
msgstr "Por favor, selecione um arquivo de origem para editar"

msgid "Could not find file %s to edit"
msgstr "Não foi encontrado o arquivo %s para editar"

#. First letter is uppercase
msgid "Go back to the main menu"
msgstr "Voltar ao menu principal"

msgid "Search for packages"
msgstr "Procurar programas ou pacotes"

#. An "exact name match" means the search term matches the entire package name
msgid "exact name match"
msgstr "o nome completo corresponde"

msgid "exact name matches"
msgstr "o nome completo corresponde"

#. A "leading name match"the search term matches  the beginning of the package
#. name
msgid "leading name match"
msgstr "correspondência do primeiro termo"

msgid "leading name matches"
msgstr "o primeiro nome corresponde"

#. An "any name match" the search term matches any part of the package name
msgid "any name match"
msgstr "qualquer nome corresponde"

msgid "any name matches"
msgstr "qualquer nome corresponde"

#. A "name or description match"  matches any part of the package name or
#. description
msgid "name or description match"
msgstr "o nome ou a descrição correspondem"

msgid "name or description matches"
msgstr "o nome ou a descrição corresponde"

#. searches <took 0.25 seconds>
msgid "searches"
msgstr "buscas"

msgid "No matches were found"
msgstr "Nenhuma correspondência foi encontrada"

msgid "Do you want to try a different search?"
msgstr "Você quer tentar uma pesquisa diferente?"

#. As in "exact match", search term matches these packages exactly
msgid "exact"
msgstr "completo"

#. As in "leading match", search term matches the beginning of these packages
msgid "leading"
msgstr "primeiro"

#. As in "any match' search term matches somewhere in the package name
msgid "any name"
msgstr "qualquer termo"

#. As in "name or description match" search term matches somewhere in the name
#. or description
msgid "description"
msgstr "descrição"

msgid "Do a different search"
msgstr "Efetuar uma busca diferente"

msgid "Search Result Menu"
msgstr "Menu de Resultados da Busca"

msgid "Please select an action"
msgstr "Por favor, selecione uma ação"

msgid "What next?"
msgstr "Qual é o próximo?"

msgid "%s packages were found"
msgstr "Foram encontrados %s pacotes"

msgid ""
"In the next step you will be shown a list of packages that you can scroll"
msgstr "Na próxima etapa será exibida uma lista de pacotes"

#. Use <up-arrow>, <down-arrow>, <page-up>, and <page-down> to scroll list
msgid "Use %s, %s, %s, and %s to scroll the list"
msgstr "Utilize %s, %s, %s e %s para mover a lista"

msgid ""
"Position the package you want near the bottom and then press %s to continue"
msgstr ""
"Posicione o pacote que você quer perto do fundo e pressione %s para "
"continuar"

#. echo
#. Press <enter> to see the list
#. The Enter key
msgid "Enter"
msgstr "Entrar/Enter"

msgid "Press %s to see the list"
msgstr "Pressione %s para ver a lista"

msgid "Use %s to go to the main menu"
msgstr "Utilize %s para ir para o menu principal"

#. Use 'r' to repeat the same search
msgid "Use %s to repeat same search"
msgstr "Utilize %s para repetir a mesma busca"

#. Use 's' to do a new search
msgid "Use %s to do new search"
msgstr "Utilize %s para fazer uma nova busca"

msgid "Press %s to skip picking a package"
msgstr "Pressione %s para prosseguir sem marcar um pacote"

msgid "Or enter the number of each package you want to mark or install"
msgstr "Ou digite o número de cada pacote que você deseja marcar ou instalar"

#. Use '-' to indicate a range of numbers
msgid "Use %s to indicate a range of numbers"
msgstr "Utilize %s para indicar um intervalo de números"

#. [Use] 'q<enter>' to go the main menu, 'r<enter>' see the results again,
#. 's<enter>' to do a new search
msgid "Use %s to go to main menu"
msgstr "Utilize %s para ir para o menu principal"

msgid "Use %s to see the results again"
msgstr "Utilize %s para exibir os resultados novamente"

msgid "Use %s to do a new search"
msgstr "Utilize %s para fazer uma nova busca"

msgid "Invalid input.  Please try again"
msgstr "A informação não é válida.  Por favor, tente novamente"

msgid "The following numbers were out of range %s"
msgstr "Os seguintes números estavam fora do intervalo%s"

msgid "No valid numbers were given.  Please try again"
msgstr "Nenhum número válido foi fornecido. Por favor, tente novamente"

msgid "Selected packages"
msgstr "Pacotes selecionados"

msgid "Could not find package number %s"
msgstr "O pacote número %s não foi encontrado"

msgid "Repeat the same search"
msgstr "Repetir a mesma busca"

msgid "Search again"
msgstr "Buscar novamente"

msgid "Go back to main menu"
msgstr "Voltar ao menu principal"

msgid "Now what?"
msgstr "E agora?"

msgid "Do a new search"
msgstr "Faça uma nova busca"

msgid "See search results again"
msgstr "Veja os resultados da pesquisa novamente"

msgid "Return to main menu"
msgstr "Voltar ao menu principal"

msgid "Remove or Purge Menu"
msgstr "Remover ou Eliminar o Menu"

msgid "Please choose an action for this package"
msgstr "Por favor, escolha uma ação para este pacote"

msgid "Un-install package [remove package but not its config files]"
msgstr ""
"Desinstalar o pacote [remover o pacote, mas não os seus arquivos de "
"configurações]"

msgid "Purge package [remove package AND its config files]"
msgstr ""
"Desinstalar (purgar) o pacote [remover o pacote e os seus arquivos de "
"configurações]"

msgid "Reinstall package [even if it is up to date]"
msgstr "Reinstalar o pacote [mesmo que esteja atualizado]"

msgid "Install package [only if it is not up to date]"
msgstr "Instalar o pacote [apenas se não estiver atualizado]"

msgid "The %s command failed"
msgstr "O comando %s falhou"

msgid "Please enter the term you want to search for"
msgstr "Por favor, digite o termo que você quer buscar"

msgid "Standard globbing wildcards %s and %s work"
msgstr "Os caracteres padrão %s e %s de coincidência aberta funcionam"

msgid "Use an empty string or %s to return to main menu"
msgstr "Utilize uma linha vazia ou %s para voltar ao menu principal"

msgid "Found"
msgstr "Encontrado"

#. View <25> <exact|leading|any name|description> results
msgid "View %s %s results"
msgstr "Exibir %s resultados %s"

msgid "Show more information about package %s"
msgstr "Exibir mais informações sobre o pacote %s"

msgid "Uninstall package %s"
msgstr "Desinstalar o pacote %s"

msgid "Unmark package %s"
msgstr "Desmarcar o pacote %s"

msgid "Mark package %s"
msgstr "Marcar o pacote %s"

msgid "Install package %s"
msgstr "Instalar o pacote %s"

msgid "Mark selected, uninstalled package %s"
msgstr "Marcar o pacote %s selecionado e não instalado"

msgid "Mark %s selected, uninstalled packages"
msgstr "Marcar %s pacotes selecionados e não instalados"

msgid "install selected uninstalled package %s"
msgstr "instalar o pacote %s selecionado e não instalado"

msgid "install %s selected uninstalled packages"
msgstr "instalar %s pacotes selecionados e não instalados"

msgid "Checking source files ..."
msgstr "Procurando os arquivos de origem ..."

msgid "Run %s now?"
msgstr "Executar o comando %s agora?"

msgid "No list files were found"
msgstr "Não foram encontrados os arquivos .list"

#. An <apt-get update> is required in order to continue
msgid "An %s is required in order to continue"
msgstr "É necessário uma %s para continuar"

msgid "It appears that at least one list file is missing"
msgstr "Parece que pelo menos um arquivo .list está faltando"

#. You should probably run <apt-get update> now
msgid "You should probably run %s now"
msgstr "Provavelmente %s deverá ser executado agora"

#. Doing <apt-get update>
msgid "Doing %s"
msgstr "Executando %s"

msgid "There was a problem running %s"
msgstr "Ocorreu um problema ao executar %s"

msgid "Do you want to continue anyway"
msgstr "Você quer continuar, mesmo assim?"

msgid "Creating database ..."
msgstr "Criando a banco de dados ..."

#. data base generation <took 12 seconds>
msgid "database generation"
msgstr "geração do banco de dados"

msgid "Updating database ..."
msgstr "Atualizando o banco de dados ..."

#. database update <took 4 seconds>
msgid "database update"
msgstr "atualização do banco de dados"

msgid "Found %s dependencies"
msgstr "Encontradas %s dependências"

msgid "Marking %s uninstalled dependencies"
msgstr "Marcando %s dependências não instaladas"

msgid "found %s dependencies"
msgstr "encontradas %s dependências"

msgid "found %s unmet dependencies"
msgstr "encontradas %s dependências orfãs"

msgid "This package and unmet dependencies will add %s Meg"
msgstr "Este pacote e as suas dependências orfãs acrescentarão %s MB"

msgid "Marking package %s"
msgstr "Marcando o pacote %s"

msgid "Unmarked %s package"
msgstr "Desmarcado %s pacote"

msgid "Unmarked %s packages"
msgstr "Desmarcados %s pacotes"

msgid "Do you want to see more detailed information?"
msgstr "Você quer ver as informações detalhadas?"

msgid "found %s unmet dependencies that will also get installed"
msgstr "encontradas %s dependências orfãs que também serão instaladas"

msgid "Do you want to install this package now?"
msgstr "Você quer instalar este pacote agora?"

msgid "Installing %s"
msgstr "Instalando %s"

msgid "Found %s un-installed marked packages"
msgstr "Encontrados %s pacotes marcados e não instalados"

msgid "Nothing to install"
msgstr "Nada para ser instalado"

msgid "Install %s package"
msgstr "Instalar %s pacote"

msgid "Install %s packages"
msgstr "Instalar %s pacotes"

msgid "The packages and unmet dependencies will add %s Meg"
msgstr "Os pacotes e as dependências orfãs acrescentarão %s MB"

msgid "Install this package?"
msgstr "Instalar este pacote?"

msgid "Install these packages?"
msgstr "Instalar estes pacotes?"

msgid "Install %s marked package"
msgstr "Instalar %s pacote marcado"

msgid "Install all %s marked packages"
msgstr "Instalar os %s pacotes marcados"

msgid "View %s marked package"
msgstr "Exibir %s pacote marcado"

msgid "View all %s marked packages"
msgstr "Exibir os %s pacotes marcados"

msgid "Unmark %s marked package"
msgstr "Desmarcar %s pacote marcado"

msgid "Unmark all %s marked packages"
msgstr "Desmarcar os %s pacotes marcados"

msgid "View or Install Marked Packages"
msgstr "Exibir ou Instalar os Pacotes Marcados"

msgid "No marked packages were found"
msgstr "Não foram encontrados pacotes marcados"

msgid "Marked packages will add %s Meg"
msgstr "Os pacotes marcados acrescentarão %s MB"

msgid ""
"In the next step you will been shown a list of packages that you can scroll"
msgstr "Na próxima etapa, você verá uma lista de pacotes"

msgid "Use %s to go the main menu"
msgstr "Utilize %s para ir para o menu principal"

msgid "Use %s to do another search"
msgstr "Utilize %s para fazer outra busca"

#. Suggested <Command Line> packages
msgid "Suggested %s Packages"
msgstr "Sugeridos %sPacotes"

msgid "No suggestions were found!"
msgstr "Não foram encontradas sugestões!"

#. error message if things are reall not working
msgid "Check for the file %s"
msgstr "Procurar o arquivo %s"

#. View <16> <***> star packages
msgid "View"
msgstr "Exibir"

#. View all <40> suggested packages
msgid "View all"
msgstr "Exibir todos os"

#. View all <25> suggested packages
msgid "suggested packages"
msgstr "pacotes/programas sugeridos"

#. View <16> <***> star packages
msgid "star packages"
msgstr "pacotes/programas com estrela (classificado)"

#. View <32> zero star packages
msgid "zero star packages"
msgstr "pacotes/programas com zero estrela (não classificado)"

msgid "Skip to the next step (don't view packages)"
msgstr "Passar para a próxima etapa (não exibir os pacotes)"

msgid "Please select which packages to view"
msgstr "Por favor, selecione os pacotes a serem visualizados"

msgid "%s packages were found (%s uninstalled)"
msgstr "Foram encontrados %s pacotes (%s não estão instalados)"

msgid "Press %s to see the list, %s to go the main menu, %s to search again"
msgstr ""
"Pressionar %s para ver a lista, %s para ir ao menu principal, %s para "
"buscar novamente"

msgid "See suggestions again"
msgstr "Voltar para ver as sugestões novamente"

msgid "Marked %s previously unmarked packages"
msgstr "Marcados %s pacotes anteriormente desmarcados"

msgid "Unmarked all %s previously marked packages"
msgstr "Desmarcados os %s pacotes anteriormente marcados"

msgid "%s package is still marked"
msgstr "%s pacote ainda está marcado"

msgid "%s packages are still marked"
msgstr "%s pacotes ainda estão marcados"

msgid "Mark %s unmarked, uninstalled package"
msgstr "Marcar %s pacote não marcado e não instalado"

msgid "Mark %s unmarked, uninstalled packages"
msgstr "Marcar %s pacotes não marcados e não instalados"

msgid "Unmark %s marked packages"
msgstr "Desmarcar %s pacotes marcados"

msgid "Checking for package upgrades ..."
msgstr "Procurando as atualizações dos pacotes ..."

msgid "There are %s packages that can be upgraded"
msgstr "Há %s pacotes que podem ser atualizados"

msgid "There are no packages to upgrade"
msgstr "Não há pacotes para serem atualizados"

msgid "System Upgrade Menu"
msgstr "Menu de Atualização do Sistema Operacional"

msgid "Exit"
msgstr "Sair"
